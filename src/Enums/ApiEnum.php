<?php
/**
 * Project jpush-server
 * file: ApiEnum.phphp
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 18:50
 */

namespace WebLinuxGame\JPush\Enums;

/**
 * 常量枚举
 * Interface ApiEnum
 * @package WebLinuxGame\JPush
 */
interface ApiEnum
{
    const IOS = 'ios';
    const ALL = 'all';
    const ANDROID = 'android';
    const OPEN_PAGE = '_open_page';
    const ERR_MISS_CODE = 0;
    const EXTRAS = 'extras';

    // 时间单位
    const TIME_UNIT_HOUR = 'HOUR';
    const TIME_UNIT_DAY = 'DAY';
    const TIME_UNIT_MONTH = 'MONTH';

    const DESC_TEXT =[
        self::IOS => '苹果',
        self::ALL => '所有',
        self::ANDROID => '安卓',
        self::OPEN_PAGE => '落地页',
        self::ERR_MISS_CODE => '缺失参数异常码',
        self::EXTRAS => '扩展参数',
    ];
}