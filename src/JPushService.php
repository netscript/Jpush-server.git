<?php
/**
 * Project jpush-server
 * file: JpushService.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 16:50
 */

namespace WebLinuxGame\JPush;

use WebLinuxGame\JPush\Enums\ApiEnum;
use WebLinuxGame\JPush\Supports\Client;
use WebLinuxGame\JPush\Supports\Configure;
use WebLinuxGame\JPush\Contracts\JPushRegister;
use WebLinuxGame\JPush\Exception\RuntimeJPushException;

/**
 * 极光推送服务
 * Class JPushService
 * @package WebLinuxGame\JPush
 */
class JPushService
{
    /**
     * @var Configure $config [配置]
     */
    protected $config;

    /**
     * 应用容器
     * @var array
     */
    protected $apps = [];

    /**
     * JPushService constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = new Configure($config);
    }

    /**
     * 构建应用客户端
     * @param string $name
     * @return Client
     * @throws RuntimeJPushException
     */
    public function client(string $name = 'default'): Client
    {
        if (!empty($this->apps[$name])) {
            return $this->apps[$name];
        }
        $config = [];
        if (!empty($this->config['apps']) && !empty($this->config['apps'][$name])) {
            $config = $this->config['apps'][$name];
        }
        if (empty($config)) {
            throw new RuntimeJPushException('missed config', ApiEnum::ERR_MISS_CODE);
        }
        if (empty($config['app_key'])) {
            throw new RuntimeJPushException('missed config : app_key', ApiEnum::ERR_MISS_CODE);
        }
        if (empty($config['master_secret'])) {
            throw new RuntimeJPushException('missed config : master_secret ', ApiEnum::ERR_MISS_CODE);
        }
        if (empty($config['log_file']) && !empty($this->config['log_file'])) {
            $config['log_file'] = $this->config['log_file'];
        }
        $this->apps[$name] = new Client($config);
        return $this->apps[$name];
    }

    /**
     * 极光推送用户与系统用户关联
     * @param array $regArr [注册极光用户客户端返回数据]
     * eg:
     * [
     *    'registration_id' => xx,// [客户端发送过来的极光用户标识id]
     *    'alias' => 'xxx', // [客户端设备别称]
     *    'tag' => ['xxx','xxx'], // [客户端设备tag]
     * ]
     * @param array $data [对应本系统用户信息]
     * @param JPushRegister|null $register [注册逻辑处理]
     * @return mixed
     * @throws RuntimeJPushException
     */
    public function register(array $regArr, array $data, JPushRegister $register = null)
    {
        if (empty($regArr)) {
            throw new RuntimeJPushException('miss param regArr', ApiEnum::ERR_MISS_CODE);
        }
        if (empty($register)) {
            if (empty($this->config->get('register'))) {
                throw new RuntimeJPushException('miss register', ApiEnum::ERR_MISS_CODE);
            }
            $class = $this->config->get('register');
            if (!class_exists($class)) {
                throw new RuntimeJPushException('register:' . $class . ' undefined', ApiEnum::ERR_MISS_CODE);
            }
            $register = new $class;
            if ($register instanceof JPushRegister) {
                throw new RuntimeJPushException('register:' . $class . ' not implement' . JPushRegister::class, ApiEnum::ERR_MISS_CODE);
            }
        }
        return $register->handle(array_merge($regArr, $data));
    }

}