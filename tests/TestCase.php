<?php
/**
 * Project jpush-server
 * file: TestCase.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 17:39
 */

namespace WebLinuxGame\JPush\Tests;

use WebLinuxGame\JPush\JPushService;
use PHPUnit\Framework\TestCase as BaseTestCase;

/**
 * 测试
 * Class TestCase
 * @package WbeLinuxGame\Sms\Tests
 */
class TestCase extends BaseTestCase
{
    private $app;
    private static $config;
    protected static $runtime;

    // 默认 配置文件
    const DEV_CONFIG_FILE = __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php';

    /**
     * 单元测试
     * TestCase constructor.
     * @param null|string $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->__init();
    }

    /**
     * 初始化测试所需
     */
    protected function __init()
    {
        if (empty($this->app)) {
            $this->initEnv();
            $this->app = new JPushService($this->config());
        }
    }

    /**
     * 获取环境
     * @return string
     */
    public function getRunTimeName()
    {
        if (is_null(self::$runtime)) {
            $runtimeFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . '.runtime';
            self::$runtime = file_get_contents($runtimeFile);
        }
        return (string)self::$runtime;
    }

    /**
     * 初始化
     * @return $this
     */
    protected function initEnv()
    {
        $env = self::getRunTimeName();
        $envFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . '.env';
        $data = parse_ini_file($envFile, true);
        if (empty($data) || empty($data[$env])) {
            return $this;
        }
        foreach ($data[$env] as $key => $value) {
            putenv("$key=$value");
        }
        return $this;
    }

    /**
     * 获取配置
     * @return array
     */
    private function config(): array
    {
        if (is_null(self::$config)) {
            $file = env("sms.config");
            if (empty($file)) {
                $file = self::DEV_CONFIG_FILE;
            }
            $this->assertTrue(file_exists($file), '配置文件未知');
            self::$config = require_once $file;
        }
        return self::$config;
    }

    /**
     * 获取应用
     * @return JPushService
     */
    public function app()
    {
        return $this->app;
    }
}