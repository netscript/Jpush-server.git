<?php
/**
 * Project jpush-server
 * file: TestJPushService.php
 * User: weblinuxgame
 * Date: 2019/6/28
 * Time: 13:04
 */

namespace WebLinuxGame\JPush\Tests\Unit;

use WebLinuxGame\JPush\Enums\ApiEnum;
use WebLinuxGame\JPush\Supports\Client;
use WebLinuxGame\JPush\Tests\TestCase;

class TestJPushService extends TestCase
{
    public function testGetClient()
    {
         $oServer =  $this->app();
         $client = $oServer->client();
         $this->assertTrue($client instanceof Client,'客户端初始化失败');
    }

    public function testPush()
    {
       $ret = $this->app()->client()->push();
       dump($ret);
       $this->assertNotEmpty($ret,'异常推送');
    }

    public function testLogOff()
    {
        $client = $this->app()->client()->logOff();
        $startTime = '2019-05-01';
        $long = 60;
        dump($client->report()->getUsers(ApiEnum::TIME_UNIT_DAY,$startTime,$long));
        $this->assertNotEmpty($client,'异常推送');
    }
}