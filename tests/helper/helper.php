<?php
/**
 * Project jpush-server
 * file: helper.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 17:36
 */


if (!function_exists('env')) {

    /**
     * 环境
     * @param string $key
     * @param null $default
     * @return array|false|null|string
     */
    function env(string $key, $default = null)
    {
        $value = getenv($key);
        return empty($value) ? $default : $value;
    }

}

if (!function_exists('dump')) {

    /**
     * 调试输出
     * @param mixed ...$args
     * @return void
     */
    function dump(...$args)
    {
        $count = func_get_args();
        if ($count == 0) {
            return;
        }
        if (1 == $count) {
            var_dump(current($args));
        }
        if ($count > 1) {
            foreach ($args as $var) {
                var_dump($var);
            }
        }
        return;
    }

}


if (!function_exists('dd')) {

    /**
     * 调试输出
     * @param mixed ...$args
     */
    function dd(...$args)
    {
        $count = func_get_args();
        if ($count == 0) {
            return exit(0);
        }
        if (1 == $count) {
            var_dump(current($args));
        }
        if ($count > 1) {
            foreach ($args as $var) {
                var_dump($var);
            }
        }
        return exit(0);
    }

}

if (!function_exists('storage_path')) {

    /**
     * 存储路径
     * @param string $path
     * @return string
     */
    function storage_path(string $path = null)
    {
        $dir = env('APP_ROOT', dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'storage';
        if (empty($path)) {
            return $dir;
        }
        if (DIRECTORY_SEPARATOR != '/' && false !== strpos($path, '/')) {
            $path = str_replace('/', DIRECTORY_SEPARATOR, $path);
        }
        if (DIRECTORY_SEPARATOR == $path[0]) {
            return $dir . $path;
        }
        return $dir . DIRECTORY_SEPARATOR . $path;
    }

}